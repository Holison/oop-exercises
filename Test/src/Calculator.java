import javax.swing.JOptionPane;

public class Calculator {
	
	private double num1;
	private double num2;
	private double ans;
	
	public Calculator() {
		
		this.ans = 0.0;
		this.num1 = 0.0;
		this.num2 = 0.0;
		
	}
	
	public void inputNum1() {
		this.num1 = Double.parseDouble(JOptionPane.showInputDialog("Enter num 1"));
	}

	public void inputNum2() {
		this.num2 = Double.parseDouble(JOptionPane.showInputDialog("Enter num 2"));
	}
	
	public void function() {
		String operator = "Choose an operator: \n" + "1. Addition (+)\n" +
				"2. Substraction (-)\n" +  "3. Multiplication (x)\n"+ "4. Division (/)\n" + "5. Modulus (%)";
		int choice = Integer.parseInt(JOptionPane.showInputDialog(operator));
		
	
			if (choice == 1) {
				this.ans = this.num1 + this.num2;
			}
			else if (choice == 2) {
				this.ans = this.num1 - this.num2;
			}
			else if (choice == 3) {
				this.ans = this.num1 * this.num2;
			}
			else if (choice == 4) {
				this.ans = this.num1 / this.num2;
			}
			else if (choice == 5) {
				this.ans = this.num1 % this.num2;
			}
		
		 
	}
	
	public void menu() {
		String prompt = "Choose an option: \n" + "1. Assign value to num1\n" +
	"2. Assign value to num2\n" +  "3. Select an Operator\n"+ "4. Display answer\n" + "5. Exit";
		int choice = Integer.parseInt(JOptionPane.showInputDialog(prompt));
		
		while (choice != 0) {
			if (choice == 1) {
				inputNum1();
			}
			else if (choice == 2) {
				inputNum2();
			}
			else if (choice == 3) {
				function();
			}
			else if (choice == 4) {
				display();
				
			}
			else if (choice == 5) {
				return;
			}
			choice = Integer.parseInt(JOptionPane.showInputDialog(prompt));
		}
	}
	
	public void display() {
		JOptionPane.showMessageDialog(null, this.ans);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
Calculator c1 = new Calculator();
c1.menu();
	}

}
