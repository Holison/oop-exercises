
public class Bank {

	private Customer [] customer;
	private int numCus;
	
	public Bank(int maxCus) {
		
		this.customer = new Customer[maxCus];
		this.numCus = 0;
	}
	
	public void addCustomer(Customer customer) {
		
		customer = this.customer[this.numCus];
		this.numCus++;
	}
	
	public void removeCustomer(Customer customer) {
		
	}
	
	public static void main(String args[])
	{
	SavingAccount mum = new SavingAccount("s123","Mercy",1000.0);
	SavingAccount dad = new SavingAccount("g234","David",2000.0);
	mum.withDraw(100.0);
	dad.deposit(150.0);
	dad.transfer(mum, 500.0);
	System.out.println("mum bal = "+mum.getBalance());
	System.out.println("dad bal = "+dad.getBalance());
	//System.out.println(mum.getDetails());
	}
	
}
