
public class CreditAccount  {

	private static final double CREDIT_LIMIT = 5000.0;
	private String id;
	private double balance;
	private String name;
	
	
	public CreditAccount(String id, String name, double balance) {
		
		this.balance = balance;
		this.id = id;
	}
	
	public void withDraw(double amount) {
		
	}
	
	public void deposit(double amount) {
		
	}
	
	public void applyFees() {
		
	}
	
	public void applyInterest() {
		
	}
	
	public String toString(String msg) {
		return this.toString(msg);
	}
	
}
