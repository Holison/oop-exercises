
public class Account {

	private String accID;
	private String name;
	private double accBalance;
	
	public Account(String id,String name, double balance) {
		this.accID = id;
		this.accBalance = balance;
		this.name = name;
		
	}
	
	public void withdraw(double amount) {
		
	}
	
	public void deposit(double amount) {
		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setID(String id) {
		this.accID = id;
	}

	public void transfer(Account mum, double amount) {
		
		
	}

	public double getBalance() {
		
		return this.accBalance;
	}
}
