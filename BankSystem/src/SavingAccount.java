
public class SavingAccount {

	private String id;
	private double balance;
	private String name;
	
	public SavingAccount(String id, String name, double balance) {
		
		this.balance = balance;
		this.id = id;
		this.name = name;
	}
	
	public void withDraw(double amount) {
		if(this.balance > amount)
			this.balance -= amount;
	}
	
	public void deposit(double amount) {
		this.balance +=amount;
	}
	
	public void transfer(SavingAccount acc, double amount) {
		if(this.balance > amount) {
			this.balance -= amount;
			acc.deposit(amount);
		}
	}
	
	public double getBalance() {
		return this.balance;
	}
	
	public String getDetails() {
		return ("Acc# Name : "+this.name + "\n" + "Acc# ID : " + this.id);
	}
}
