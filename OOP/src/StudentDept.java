
import javax.swing.JOptionPane;

public class StudentDept {

	private String name;
	private String headLec;
	private int numStu;
	private Student [] stu;
	
	
	public StudentDept(String deptName, String headLec, int maxStu) {
		this.headLec = headLec;
		this.name = deptName;
		this.stu = new Student [maxStu];
		this.numStu = 0;
	}
	
	public void addStuds(Student stud) {
		//Student stu = new Student("mike","M",336);
		this.stu[this.numStu] = stud;
		this.numStu++;
	}
	
	public void displayStuds() {
		for(int i =0; i < this.stu.length;i++) {
			JOptionPane.showMessageDialog(null, "Dept Name : " + getName() + "\n" + "Head Lecturer : " + getHead());
			JOptionPane.showMessageDialog(null,"Name : " + this.stu[i].getName() + "\n" +
		"Gender : " + this.stu[i].getGender() + "\n" + 
		"ID : " +	this.stu[i].getID());
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getHead() {
		return this.headLec;
	}
	
	public static void main(String[] args) {
		
		String name = JOptionPane.showInputDialog("Enter Department name: ");
		String head = JOptionPane.showInputDialog("Enter Head of Dept name: ");
		int maxStu = Integer.parseInt(JOptionPane.showInputDialog("Enter number of students to add: "));
		
		StudentDept bp = new StudentDept(name,head,maxStu);
		//Student stu = new Student("mike","M",336);
		Student [] stu = new Student[maxStu];
		for(int i=0; i<stu.length;i++) {
			String stuName = JOptionPane.showInputDialog("Enter Student name: " + (i+1));
			String gender = JOptionPane.showInputDialog("Enter Student Gender name: " + (i+1));
			int id = Integer.parseInt(JOptionPane.showInputDialog("Enter Student number: " + (i+1)));
			
			stu[i] = new Student(stuName,gender,id);
			bp.addStuds(stu[i]);
		
		}
		
		bp.displayStuds();
	}
}
