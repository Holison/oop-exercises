

public class Student {

	private String name;
	private String gender;
	private int id;
	
	public Student(String stuName, String stuGender, int stuID) {
		
		this.gender = stuGender;
		this.id = stuID;
		this.name = stuName;
	}
	
	
	public String getName() {
		return this.name;
	}
	
	
	public String getGender() {
		return this.gender;
	}
	
	
	
	public int getID() {
		return this.id;
	}
	
	
}
