
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Enemy {

	private int width;
	private int height;
	private Goal goal;
	private BufferedImage enemyAlive;
	private BufferedImage enemyDead;
	private BufferedImage imageCurrent;
	
	public Enemy(GameManager board, int x, int y) {
		this.height = y;
		this.width = x;
		this.goal = new Goal(board.getGoal().getX(), board.getGoal().getY());
		
		try {
			this.enemyAlive = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Turtle.png"));
			this.enemyDead = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Turtledead.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.imageCurrent = this.enemyAlive;
		
	}

	public void performAction() {
		
		if(this.imageCurrent == this.enemyAlive) {
			
			if (this.height <= this.goal.getY()) {
				this.height = this.height + 2;
				this.width = this.width + 1;
			}
			else if(this.height >= this.goal.getY()) {
				this.height = this.height - 2;
				this.width = this.width + 1;
			}
			//this.width = this.width + 1 ;
			//this.height = this.height + 1;
		}
		
	}

	public void die() {
		
		this.imageCurrent = this.enemyDead;
	}

	public int getX() {
		
		return this.width;
	}

	public int getY() {
		
		return this.height;
	}

	public BufferedImage getCurrentImage() {
	
		return this.imageCurrent;
	}
}
