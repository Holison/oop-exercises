 
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Player {

	private int width;
	private int height;
	private char key;
	private boolean set;
	private BufferedImage playerAlive;
	private BufferedImage playerDead;
	private BufferedImage imageCurrent;
	
	public Player(int x, int y) {
		this.height = y;
		this.width = x;
		
		
		try {
			this.playerAlive = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Bird.png"));
			this.playerDead = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Birddead.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.imageCurrent = this.playerAlive;
		
	}

	public void performAction() {
		
		if(this.key == 'L' && this.set == true) {
			this.width = this.width - 5;
		}
		else if(this.key == 'R' && this.set == true) {
			this.width = this.width + 5;
		}
		else if(this.key == 'U' && this.set==true) {
			this.height = this.height - 5;
		}
		else if(this.key == 'D' && this.set == true) {
			this.height = this.height + 5;
		}
	}

	public int getX() {
		
		return this.width;
	}

	public int getY() {
		
		return this.height;
	}

	public void die() {
		
		this.imageCurrent = this.playerDead;
	}

	public  BufferedImage getCurrentImage() {
		
		return this.imageCurrent;
	}

	public void setKey(char c, boolean b) {
		this.key = c;
		this.set = b;
		
	}
}

