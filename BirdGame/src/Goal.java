
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Goal {

	private int width;
	private int height;
	private BufferedImage fishAlive;
	private BufferedImage fishDead;
	private BufferedImage imageCurrent;
	
	public Goal(int x, int y) {
		this.height = y;
		this.width = x;
		
		
		try {
			this.fishAlive = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Fish.png"));
			this.fishDead = ImageIO.read(new File("C:/Users/Home/Desktop/workspace/media/Fishdead.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.imageCurrent = this.fishAlive;
		
	}

	public int getX() {
		
		return this.width;
	}

	public  BufferedImage getCurrentImage() {
		
		return this.imageCurrent;
	}

	public int getY() {
		
		return this.height;
	}

	public void die() {
		
		this.imageCurrent = this.fishDead;
	}
}
