package control;

import robot.Robot;

//Robot Assignment for Programming 1 s1 2018
//Adapted by Caspar and Ross from original Robot code in written by Dr Charles Thevathayan
public class RobotControl implements Control
{
	// we need to internally track where the arm is
	private int height = Control.INITIAL_HEIGHT;
	private int width = Control.INITIAL_WIDTH;
	private int depth = Control.INITIAL_DEPTH;
	

	private int[] barHeights;
	private int[] blockHeights;

	private int lastBlock = 0;
	private int b3=0;
	private int maxBar;
	private int count1 =0;
	private int count2=0;
	private int srcHeight;
//	private int barIndex;
	private int count3 = 0;
	
	
	private Robot robot;

	// called by RobotImpl
	@Override
	public void control(Robot robot, int barHeightsDefault[], int blockHeightsDefault[])
	{
		this.robot = robot;

		// some hard coded init values you can change these for testing
		this.barHeights = new int[] {7,3,1,7,5,3,2};
		this.blockHeights = new int[] { 3,1,2,3,1,1 };

		// initialise the robot
		robot.init(this.barHeights, this.blockHeights, height, width, depth);

		this.maxBar = this.barHeights[0];
		for(int x=0; x<this.barHeights.length;x++) {
			if(this.barHeights[x] > this.maxBar) {
				this.maxBar = this.barHeights[x];
			}
		}
		
		//move all blocks in array
		
		int size = this.blockHeights.length;
		this.srcHeight = srcHeight();
		for(int i = size - 1; i >= 0; i--) {
			lastBlock = this.blockHeights[i];
			//System.out.println(lastBlock);
		//this.sumHeight += lastBlock;
		
		//System.out.println("Source Height: "+sumHeight);
		
		//System.out.println("Source height is : " +srcHeight);
		
			
		//this.barIndex = this.barHeights[count3];
		//System.out.println(this.barIndex);
		
		
		// a simple private method to demonstrate how to control robot
		// note use of constant from Control interface
		// You should remove this method call once you start writing your code
		
		extendToWidth(Control.MAX_WIDTH);
		contractArm(Control.MIN_WIDTH);
		armLower();
		armRaise(Control.MIN_DEPTH);
		armDown();
		//extendToWidth(Control.MAX_WIDTH);
		
		}	
		// ADD ASSIGNMENT PART A METHOD CALL(S) HERE

	}

	// simple example method to help get you started
	private void extendToWidth(int width){	
		while (this.width < width)
		{
			armDown();
			robot.extend();
			this.width++;
		}
		
		pick();
	}

	// WRITE THE REST OF YOUR METHODS HERE!

	public void pick() {
		//int i=0;
		if(this.srcHeight < this.maxBar) {
			while(this.srcHeight + this.depth + Control.MIN_WIDTH < this.height) {
				robot.lower();
				this.depth++;
			}
 		}
		else if(this.srcHeight < this.maxBar + this.lastBlock) {
			while(this.srcHeight + this.depth + Control.MIN_WIDTH < this.height) {
				robot.lower();
				this.depth++;
			}
		}
//		else if(this.srcHeight > this.maxBar) {
//			while()
//		}
		
	robot.pick();
	armRaise(Control.MIN_DEPTH);
	this.srcHeight -= this.lastBlock;
	}
	
	public void drop() {
		robot.drop();
	}
	
	//change this to a single condition
	public void contractArm(int width) {
		while(this.width > width) {
			if(this.lastBlock == Control.DEST1_COLUMN) {
				armUp();
				robot.contract();
				
			} else if(this.lastBlock == Control.DEST2_COLUMN) {
				width = Control.DEST2_COLUMN;
				armUp();
				robot.contract();
			}
			else if(this.lastBlock == Control.FIRST_BAR_COLUMN) {
				
				width = Control.FIRST_BAR_COLUMN + this.b3 ;
				armUp(); 
				robot.contract();
				
			}
			
			this.width--;
		}
		//this.count3 ++;
	}
	
	public void armLower() {
		if(this.lastBlock == Control.DEST1_COLUMN) {
		while(this.depth + this.lastBlock +this.count1 + Control.MIN_WIDTH < this.height ) {
				
			robot.lower();
			this.depth++;
			
		}
		this.count1 += this.lastBlock;
		} 
		else if(this.lastBlock == Control.DEST2_COLUMN) {
			
			while(this.depth + this.lastBlock +this.count2 + Control.MIN_WIDTH < this.height ) {
				
				robot.lower();
				this.depth++;
				
			}
			this.count2 += this.lastBlock;
			
		} else if(this.lastBlock == Control.FIRST_BAR_COLUMN) {
			while(this.depth + this.lastBlock + this.barHeights[this.b3] + Control.MIN_WIDTH < this.height ) {
				
				robot.lower();
				this.depth++;
			}
			this.b3++;
			this.count3 += this.lastBlock;
		}
		drop();
		
		//(this.depth + Control.MIN_WIDTH + this.lastBlock ) < this.height
	}
	
	public void armRaise(int minDepth) {
		while(this.depth > minDepth) {
			robot.raise();
			this.depth--;
		}
		
	}
	
	
	public void armDown() {
		if(this.count2 > this.maxBar ) {
			while(this.height > this.count2 + Control.MIN_WIDTH ) {
				robot.down();
				this.height--;
			}
		} else if (this.count1 > this.maxBar) {
			while(this.height > this.count1 + Control.MIN_WIDTH  ) {
				robot.down();
				this.height--;
			}
		}
//		else if(this.maxBar > this.srcHeight) {
//			while(this.height > this.maxBar + Control.MIN_WIDTH) {
//				robot.down();
//				this.height--;
//			}
//		}
		else if(this.maxBar + this.count3 > this.srcHeight) {
			while(this.height > this.maxBar + this.count3 + Control.MIN_WIDTH) {
				robot.down();
				this.height--;
			}
		}
		
			else if(this.height > this.srcHeight){
				while(this.height > this.srcHeight + Control.MIN_WIDTH) {
					robot.down();
					this.height--;
				}
			}
		
		
		
//		while (this.height > this.srcHeight + Control.MIN_WIDTH &&
//				this.maxBar < this.srcHeight) {
//			robot.down();
//			this.height--;
//		}
			
	}
	
	public void armUp() {
		if(this.srcHeight + this.lastBlock > this.maxBar) {
		while(this.maxBar + Control.MIN_WIDTH + this.lastBlock > this.height) {
			robot.up();
			this.height++;
		}
	}
//		else if(this.b3 > 0 && this.lastBlock == Control.FIRST_BAR_COLUMN) {
//		while(this.maxBar + this.count3 + this.lastBlock + Control.MIN_WIDTH > this.height) {
//			break;
//		}
//	}
		else if(this.srcHeight + this.lastBlock <= this.maxBar) {
			if(this.lastBlock != Control.FIRST_BAR_COLUMN) {
		while(this.maxBar + Control.FIRST_BAR_COLUMN + this.lastBlock + Control.MIN_WIDTH > this.height) {
			robot.up();
			this.height++;
		}
			}
			else if(this.lastBlock == Control.FIRST_BAR_COLUMN) {
				
			}
		while(this.count1 + this.lastBlock + Control.MIN_WIDTH > this.height) {
			robot.up();
			this.height++;
		}
//		while(this.maxBar + Control.FIRST_BAR_COLUMN + this.lastBlock + Control.MIN_WIDTH > this.height) {
//			robot.up();
//			this.height++;
//		}
	}
	}
	
	public int srcHeight() {
			int sum=0;
		for(int i=0; i< blockHeights.length;i++) {
			sum += blockHeights[i];
			}
		return sum;
	}
	
	
	
}
