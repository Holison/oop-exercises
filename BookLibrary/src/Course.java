
public class Course {

	private String name;
	private String id;
	private Student [] stu;
	private int numStu;
	
	public Course(String cosName, String cosID) {
		int max = 100;
		this.id = cosID;
		this.name = cosName;
		this.stu = new Student[max];
		this.numStu = 0;
		
	}
	
	public void addStu(Student stud, Course cos) {
		this.stu[this.numStu] = stud;
		this.numStu++;
		System.out.println("Student added in " + cos.getID() );
	}
	
	public String getID() {
		return this.id;
	}
	
	public String toString() {
		//return String.format("Course Name : %s\nCourse ID : %s", this.name,this.id);
		StringBuffer buffer = new StringBuffer();
		buffer.append(String.format("CourseID: %s, Course Name: %s", id, name));
		buffer.append('\n');
		// note that we use Student.toString() here via students[i]
		for (int i = 0; i < numStu; i++)
			buffer.append(String.format("Enrollment: %d, student: %s\n", i+1,
					stu[i]));

		return buffer.toString();
	}
	
}
