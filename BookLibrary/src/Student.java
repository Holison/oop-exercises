
public class Student {

	private String name;
	private int id;
	
	public Student(String stuName,int stuId ) {
		this.id = stuId;
		this.name = stuName;
	}
	
	private String getName() {
		return this.name; 
	}
	
	public String toString() {
		return String.format("Student Name : %s\nStudent Number : %s", this.name,this.id);
	}
	
}
