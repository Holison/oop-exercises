import javax.swing.JOptionPane;

public class Album {

	private String albumName;
	private Song [] songs;
	private int releaseYear;
	private String publisher;
	private int numSongs;
	
	public Album(String albumName, int maxNumSongs) {
		this.albumName = albumName;
		this.publisher = "";
		this.releaseYear=0;
		this.numSongs=0;
		this.songs = new Song[maxNumSongs];
	}
	
	public String getAlbumName() {
		return this.albumName;
	}
	
	public void setAlbumName(String name) {
		this.albumName = name;
	}
	
	public void setYear(int year) {
		this.releaseYear = year;
	}
	
	public int getYear() {
		return this.releaseYear;
	}
	
	public String getPublisher() {
		return this.publisher;
	}
	
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public void addSong(Song newSong) {
			this.songs[this.numSongs] = newSong;
			this.numSongs++;
	
		JOptionPane.showMessageDialog(null, "New Song: " + newSong.getTitle() + "\nBy: " + newSong.getArtist() +
				"\nAudioFile: " + newSong.getAudio());
	}
	
	public Song getSongByTitle(String Target) {
		for(int i = 0; i < numSongs; i++) {
			if(this.songs[i].getTitle().contains(Target)) {
				return this.songs[i];
			}
		}
		return null;
	}
	
//	public static void main(String[] args) {
//		Album a1 = new Album("thanks", 2);
//		a1.addSong(new Song("music","mp3"));
//		a1.addSong(new Song("thanks","mp3"));
//	}

}
