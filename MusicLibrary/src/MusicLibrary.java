import javax.swing.JOptionPane;

public class MusicLibrary {

	private Album[] listAlbum;
	private int numAlbum;
	
	
	public MusicLibrary(int maxAlbum) {
		
		this.listAlbum = new Album[maxAlbum];
		this.numAlbum = 0;
	}
	
	public void createAlbum(Album musicAlbum) {
		
			this.listAlbum[this.numAlbum] = musicAlbum;
			this.numAlbum++;
		
		JOptionPane.showMessageDialog(null, "New Album: " + musicAlbum.getAlbumName() + "\nPublsiher: " + 
		musicAlbum.getPublisher() + "\nYear Release: " + musicAlbum.getYear());
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int album = Integer.parseInt(JOptionPane.showInputDialog("How many albums do u want to create?"));

		MusicLibrary ml = new MusicLibrary(album);
		
		
		//String albumName = JOptionPane.showInputDialog("What's the name of the album?");
		//int songNum = Integer.parseInt(JOptionPane.showInputDialog("How many songs in this album?"));
		//ml.createAlbum(new Album(albumName,songNum));
		
		Album [] a = new Album[album];
		for(int i =0; i<a.length;i++) {
			String albumName = JOptionPane.showInputDialog("What's the name of the album " + (i+1)+ "?");
			String pub = JOptionPane.showInputDialog("Who's the publisher?");
			int year = Integer.parseInt(JOptionPane.showInputDialog("what year was the album release?"));
			int songNum = Integer.parseInt(JOptionPane.showInputDialog("How many songs in this album?"));
			a[i] = new Album(albumName,songNum);
			a[i].setPublisher(pub);
			a[i].setYear(year);
			ml.createAlbum(a[i]);
			
			Song [] sound = new Song[songNum];
			for(int j =0;j<sound.length;j++) {
				String song = JOptionPane.showInputDialog("Enter name for Song " + (j+1));
				String artist = JOptionPane.showInputDialog("Enter Artist for Song " + (j+1));
				String audio = JOptionPane.showInputDialog("Enter path to file for Song " + (j+1));
				
				sound[j] = new Song(song,audio);
				sound[j].setArtist(artist);
				a[i].addSong(sound[j]);
			}
			
		}
		
		
	}

}
