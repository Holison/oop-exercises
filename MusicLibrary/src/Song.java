
public class Song {

	private String title;
	private String artist;
	private int duration;
	private String audioFile;
	private int rating;
	
	public Song(String songTitle, String audio) {
		this.title = songTitle;
		this.audioFile = audio;
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public void setTitle(String songTitle) {
		this.title = songTitle;
	}
	
	public void setArtist(String artistName) {
		this.artist = artistName;
	}
	
	public String getArtist() {
		return this.artist;
	}
	
	public String getAudio() {
		return this.audioFile;
	}
	
	public void setAudio(String file) {
		this.audioFile = file;
	}
	
	 public void setDuration(int time) {
		 this.duration = time;
	 }
	 
	 public int getDuration() {
		 return this.duration;
	 }
	 
	 public void setRating(int rate) {
		 this.rating = rate;
	 }
	 
	 public int getRating() {
		 return this.rating;
	 }

}
